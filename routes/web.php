<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::resource('imagenes','ImagenesController');
Route::resource('eventos','EventosController');

Route::get('/', 'EventosController@index')->name('Eventos');
Route::get('/about', 'AshvaController@about')->name('Sobre ASHVA');
Route::get('/historia', 'AshvaController@historia')->name('Historia');
Route::get('/vacaciones-en-paz', 'AshvaController@vep')->name('Vacaciones en Paz');
Route::get('/asociate', 'AshvaController@asociate')->name('Asóciate');
Route::get('/donativos', 'AshvaController@donativos')->name('Donativos');


Route::group(['prefix' => 'admin3012', 'middleware' => ['admin']], function () {
    
});