@extends('layouts.layout')

@section('title','Vacaciones en Paz')

@section('content')

    <!-- Vacaciones en Paz -->
    <section id="VacaPaz">
        <div class="contenedor">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Vacaciones en Paz</h2>
                    <h3 class="section-subheading text-muted">Desde 1976 cada verano durante los meses de Julio y Agosto, las asociaciones del pueblo saharahui traen ni&ntilde;os de 8 a 12 a&ntilde;os de los campos de refugiados para que conozcan una vida normal lejos de la ocupaci&oacute;n militar de su tierra y las condiciones climatol&oacute;gicas insoportables del verano en el inh&oacute;spito desierto argelino donde se ubican los campamentos de refugiados.</h3>
                </div>
				<div class="center-h">
					<img src="images/asociacion/recepcion_ninos.jpg" class="img-responsive" alt="">
                </div>
            </div>
			<div class="redaction">
				<div class="panel panel-body">
					<!--<img src="images/asociacion/chicasamigas.png" alt="Img">-->
					<h2 align=center>&iquest;Deseas acoger?</h2>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						No es necesario estar casados ni tener hijos peque&ntilde;os. Basta con desearlo.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Si deseas acoger un ni&ntilde;o o ni&ntilde;a este verano tienes las siguientes opciones:<br>
						<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Haz clic en la pesta&ntilde;a "Contacto" o <a class="page-scroll" href="#contact">aqu&iacute;</a>.<br>-->
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Ll&aacute;manos: 657 805 775<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Escribe un email: saharacyl@gmail.com<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Imprime el siguiente formulario y entr&eacute;ganoslo personalmente o por carta: <a href="documentos/SolicitudvPaz.pdf" target="_blank">formulario</a> 
					</p>
				</div>
				<div class="panel panel-body">
					<!--<img src="images/asociacion/chicosenespera.png" alt="Img">-->
					<h2 align=center>&iquest;Por qu&eacute; acoger a un ni&ntilde;o o ni&ntilde;a saharahui?</h2>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Acoger a un ni&ntilde;o saharaui es una experiencia &uacute;nica y exenta de dificultades. Cualquier familia que decida realizar este esfuerzo debe saber que, en realidad, no est&aacute; haciendo s&oacute;lo una buena obra. Todos o&iacute;mos en multitud de ocasiones que es una l&aacute;stima traer a estos ni&ntilde;os a Espa&ntilde;a, para que durante dos meses gocen de comodidades y disfruten de lo m&aacute;s elemental y luego, al volver, retornen de nuevo a una vida llena de carencias.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 En realidad no es as&iacute;. Es muy importante que estos ni&ntilde;os vengan a Espa&ntilde;a durante el verano. El programa "Vacaciones en paz" se pone en marcha en la &eacute;poca en la que los ni&ntilde;os espa&ntilde;oles no tienen colegio, lo que permite a los ni&ntilde;os saharauis y a los espa&ntilde;oles un intercambio cultural extraordinario y les da a unos y a otros la oportunidad de contactar con una cultura distinta, con personas diferentes, con otras costumbres. Conocer esta realidad har&aacute; m&aacute;s tolerantes a nuestros hijos.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 Pero no es esto lo m&aacute;s importante. En los dos meses de verano se producen en la "Hamada" argelina, la zona en la que se ubican los campos de refugiados saharauis, temperaturas de m&aacute;s de 50 &ordm; C a la sombra. La deshidrataci&oacute;n, la escasez de alimentos provoca un empeoramiento de la salud de estos ni&ntilde;os, que no pueden crecer en las mismas condiciones que cualquier otro.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 Pero a&uacute;n hay m&aacute;s. Quiz&aacute; el aspecto m&aacute;s importante de la campa&ntilde;a "Vacaciones en paz" es el programa sanitario que los responsables de la campa&ntilde;a han dise&ntilde;ado. Este programa, que se realiza en colaboraci&oacute;n con las autoridades sanitarias de cada Comunidad Aut&oacute;noma y de cada Ayuntamiento, permite que todos estos ni&ntilde;os sean sometidos a reconocimientos m&eacute;dicos cada vez que vienen a Espa&ntilde;a. Estos reconocimientos anuales hacen posible que los ni&ntilde;os saharauis disfruten de una vigilancia peri&oacute;dica de su salud, lo que les da, al mismo tiempo, m&aacute;s oportunidades para tener un crecimiento normal.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 Durante los dos meses que est&aacute;n en Espa&ntilde;a, los ni&ntilde;os saharauis son sometidos a revisiones de la vista (da&ntilde;ada por el tremendo sol del Sahara), del o&iacute;do, de sangre y orina. Cualquier anomal&iacute;a detectada por los m&eacute;dicos permite dise&ntilde;ar el tratamiento id&oacute;neo para que este ni&ntilde;o pueda superarla con &eacute;xito.
					</p>
				</div>
				<div class="panel panel-body">
					<!--<img src="images/asociacion/grupochicas.png" alt="Img">-->
					<h2 align=center>Dos meses de convivencia</h2>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						A finales de junio, unos 6000 ni&ntilde;os y ni&ntilde;as llegar&aacute;n a Espa&ntilde;a. Hacen falta familias que les acojan en algunas comunidades aut&oacute;nomas espa&ntilde;olas. Durante dos meses, estos ni&ntilde;os saharauis ser&aacute;n sometidos a los reconocimientos m&eacute;dicos antes se&ntilde;alados, ocupar&aacute;n un espacio en nuestras casas, convivir&aacute;n con nuestros hijos, compartir&aacute;n un espacio importante de nuestra vida.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 En definitiva: Acoger a un ni&ntilde;o exige un esfuerzo que s&oacute;lo se puede realizar con generosidad y con comprensi&oacute;n. El ni&ntilde;o saharaui tiene otras costumbres, otra cultura, otro idioma. &Eacute;l, sin duda, realizar&aacute; un esfuerzo enorme por adaptarse a nosotros, pero nosotros tambi&eacute;n debemos acogerle como merece.
					</p>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 Los ni&ntilde;os saharauis son personas extraordinarias: serviciales, amables, obedientes, tolerantes... Se trata de ni&ntilde;os que van a poner mucho de su parte. La convivencia, como todas, tendr&aacute; en algunos casos, choques, como en cualquier convivencia, pero no estar&aacute; exenta de enormes satisfacciones.
					</p>
				</div>
			</div>
        </div>
    </section>	

@endsection