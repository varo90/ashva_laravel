@extends('layouts.layout')

@section('title','Sobre ASHVA')

@section('content')

    <!-- Services Section -->
    <section id="inicio">
        <div class="contenedor">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1>ASHVA</h1>
                    <div align="center"><img src="images/asociacion/logo.jpg" class="img-responsive" alt=""></div>
                    <h5 class="section-subheading text-muted">Asociaci&oacute;n Cultural Amigos del Pueblo Saharahui de Castilla y Le&oacute;n en Valladolid</h4><br>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <img src="images/asociacion/camp.jpg" class="img-responsive" alt="">
                    <!--<span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>-->
                    <h4 class="service-heading">&iquest;Sabes lo que es un campo de refugiados?</h4>
                    <p class="text-muted">Los ni&ntilde;os y ni&ntilde;as de los CAMPOS DE REFUGIADOS SAHARAUIS DE TINDUF lo saben muy bien porque llevan viviendo en ellos desde que nacieron, en mitad del inh&oacute;spito desierto argelino, expulsados de su tierra la antigua colonia espa&ntilde;ola del S&aacute;hara Occidental, actualmente ocupada por Marruecos.</p>
                </div>
                <div class="col-md-4">
                    <img src="images/asociacion/chiquillas.jpg" class="img-responsive" alt="">
                    <!--<span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>-->
                    <h4 class="service-heading">&iquest;Quieres acoger este verano?</h4>
                    <p class="text-muted">A trav&eacute;s del proyecto <a class="page-scroll" href="vacacionesEnPaz.php">VACACIONES EN PAZ</a>, las Asociaciones de Amigos del Pueblo Saharaui de toda Espa&ntilde;a ayudamos a que los ni&ntilde;os y niñas salgan durante los meses de verano de esas duras condiciones y pasen el verano en familias de acogida espa&ntilde;olas.</p>
                </div>
                <div class="col-md-4">
                    <img src="images/asociacion/jaima.jpg" class="img-responsive" alt="">
                    <!--<span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>-->
                    <h4 class="service-heading">&iquest;Nos ayudas a ayudarles?</h4>
                    <p class="text-muted">A lo largo de todo el a&ntilde;o enviamos a los campamentos saharauis alimentos, ropa y medicinas y promovemos proyectos de desarrollo y colaboraci&oacute;n en materias como la educaci&oacute;n y la sanidad. Puedes ayudar con un <a class="page-scroll" href="donativo.php">donativo</a> y/o trayendo libros, ropa o juguetes. Tambi&eacute;n puedes involucrarte personalmente <a class="page-scroll" href="socio.php">haciéndote socio/a</a>.</p>
                </div>
            </div>
        </div>
    </section>

@endsection