@extends('layouts.layout')

@section('title','Dona')

@section('content')

    <!-- Donativos -->
    <section id="donativos">
        <div class="contenedor">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Donativos</h2>
                    <h3 class="section-subheading text-muted">La financiaci&oacute;n es vital para el funcionamiento de la asociaci&oacute;n y el poder llevar a cabo los distintos proyectos.
					<br>Agradecemos mucho su donaci&oacute;n, y se lo agradecemos tambi&eacute;n de parte de las personas que viven en los campamentos de refugiados.</h3>
				</div>
            </div>
			<div class="panel panel-default" align="center">
				<h2>DONATIVO ECON&Oacute;MICO</h2>
				<div align="center">
					<img src="images/asociacion/donation_heart.jpg" class="img-responsive" alt="">
				</div>
				<p>Los datos de la cuenta son los siguientes:<br>
				Entidad: ING Direct<br>
				IBAN: ES65 1465 0310 3219 0040 2738<br>
				N&uacute;mero de Cuenta: 1465-0310-32-1900402738</p>
				Tambi&eacute;n puede hacerlo r&aacute;pidamente con su cuenta Paypal o tarjeta:
				<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=T9SF28U9LJ2HS" target="_blank">
                    <img src="images/paypal_donate.gif" class="img-responsive img-centered" alt="">
                </a>
			</div>
			<div class="panel panel-default" align="center">
				<h2>DONATIVO EN ESPECIE</h2>
				<div align="center">
					<img src="images/asociacion/donation-box.jpg" class="img-responsive" alt="">
				</div>
				<p>Ropa, comida, material escolar, ordenadores, m&oacute;viles, juguetes... En los campamentos de refugiados dependen de ello.<br>
				Cada ciertos meses enviamos un cami&oacute;n con todo lo que podemos meter en &eacute;l, y nada se desperdicia salvo la comida perecedera.<br>
				Una prenda desgastada, un ordenador o m&oacute;vil antiguo pueden marcar la diferencia.
				Si deseas aportar alg&uacute;n bien, contacta con nosotros:<br><br>
				Tel&eacute;fono: 657 805 775 <br>
				Correo: saharacyl@gmail.com
			</div>
        </div>
    </section>	

@endsection