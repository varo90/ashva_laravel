<!-- Colaboradores -->
<aside class="clients">
    <div class="contenedor">
    <h2>Colaboradores:</h2>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <a href="http://www.diputaciondevalladolid.es/" target="_blank">
                    <img src="{{ asset('images/logos/diputacionValladolid.jpg') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="https://www.valladolid.es/" target="_blank">
                    <img src="{{ asset('images/logos/ayuntamiento valladolid.jpg') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="http://www.ayto-medinadelcampo.es/" target="_blank">
                    <img src="{{ asset('images/logos/ayto_medina.png') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="https://www.jcyl.es/" target="_blank">
                    <img src="{{ asset('images/logos/logoJCyL.jpg') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <a href="http://www.zoologicolaeradelasaves.es/" target="_blank">
                    <img src="{{ asset('images/logos/la_era_de_las_aves.jpg') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="http://clinicadentalpiccolaplaza.com/" target="_blank">
                    <img src="{{ asset('images/logos/piccola-plaza.png') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6" target="_blank">
                <a href="http://www.clinicabaviera.com/clinica-oftalmologica/valladolid">
                    <img src="{{ asset('images/logos/clinica_baviera.jpg') }}" class="img-responsive img-centered" alt="">
                </a>
            </div>
        </div>
    </div>
</aside>