@extends('layouts.layout')

@section('title',$evento->titulo)

@section('content')

<div class="contenedor">
    <div class="row">
      <!-- Latest Posts -->

      <!-- https://demo.bootstrapious.com/blog/1-2-1/ -->
      <main class="post blog-post col-lg-12"> 
        <div class="container">
            <h1>{{ $evento->titulo }}</h1>
            <p class="lead">{!! $evento->desc_corta !!}</p>
          <div class="post-single">
            <img src="{{ asset('images/portfolio/'.$evento->imagen) }}" class="img-fluid">
            <div class="post-details">
              <div class="post-body">
                <p>{!! $evento->desc_larga !!}</p>
                {{-- <p> <img src="https://d19m59y37dris4.cloudfront.net/blog/1-2-1/img/featured-pic-3.jpeg" alt="..." class="img-fluid"></p> --}}
                {{-- <h3>Lorem Ipsum Dolor</h3>
                <p>div Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda temporibus iusto voluptates deleniti similique rerum ducimus sint ex odio saepe. Sapiente quae pariatur ratione quis perspiciatis deleniti accusantium</p>
                <blockquote class="blockquote">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                  <footer class="blockquote-footer">Someone famous in
                    <cite title="Source Title">Source Title</cite>
                  </footer>
                </blockquote> 
                <p>quasi nam. Libero dicta eum recusandae, commodi, ad, autem at ea iusto numquam veritatis, officiis. Accusantium optio minus, voluptatem? Quia reprehenderit, veniam quibusdam provident, fugit iusto ullam voluptas neque soluta adipisci ad.</p>
              --}}
              </div>
            </div>
          </div>
        </div>
      </main>
  </div>

@endsection