@extends('layouts.layout')

@section('title','Imágenes')

@section('content')

    <div class="contenedor">
        <h1>IMÁGENES DE LOS CAMPAMENTOS DE REFUGIADOS</h1>
        <h5 class="section-subheading text-muted">Si tienes imágenes que quieres publicar aquí puedes pasárnoslas a: saharacyl@gmail.com</h5>
        
        @if(Auth::check() && Auth::user()->role_id == 1)

            @include('commons/errors')

            <form class="form-events" method="POST" action="/imagenes" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="author">Subir imágenes:</label>
                    <input required type="file" class="form-control" name="images[]" placeholder="address" multiple>
                </div>
                <div>
                    <button  class="btn btn-primary" type="submit">Subir imágenes</button>
                </div>
            </form>
        @endif
            
        <div class="separar-50"></div>
        <div class="row">
            <div class="row">
                @foreach($imagenes as $imagen)
                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                        data-image="images/camp/{{$imagen->getFilename()}}"
                        data-target="#image-gallery">
                            <img class="img-thumbnail" src="images/camp/{{$imagen->getFilename()}}" alt="">
                        </a>

                        @if(Auth::check() && Auth::user()->role_id == 1)
                            <form method="POST" action="/imagenes/{{$imagen->getFilename()}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger btn-sm delete_imagen" type="submit"><i class="fa fa-times"></i></button>
                            </form>
                        @endif
                    </div>
                @endforeach
            </div>
    
    
            <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="image-gallery-title"></h4>
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                            </button>
    
                            <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let modalId = $('#image-gallery');

        $(document)
        .ready(function () {

            loadGallery(true, 'a.thumbnail');

            //This function disables buttons when needed
            function disableButtons(counter_max, counter_current) {
            $('#show-previous-image, #show-next-image')
                .show();
            if (counter_max === counter_current) {
                $('#show-next-image')
                .hide();
            } else if (counter_current === 1) {
                $('#show-previous-image')
                .hide();
            }
            }

            /**
             *
             * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
             * @param setClickAttr  Sets the attribute for the click handler.
             */

            function loadGallery(setIDs, setClickAttr) {
            let current_image,
                selector,
                counter = 0;

            $('#show-next-image, #show-previous-image')
                .click(function () {
                if ($(this)
                    .attr('id') === 'show-previous-image') {
                    current_image--;
                } else {
                    current_image++;
                }

                selector = $('[data-image-id="' + current_image + '"]');
                updateGallery(selector);
                });

            function updateGallery(selector) {
                let $sel = selector;
                current_image = $sel.data('image-id');
                $('#image-gallery-title')
                .text($sel.data('title'));
                $('#image-gallery-image')
                .attr('src', $sel.data('image'));
                disableButtons(counter, $sel.data('image-id'));
            }

            if (setIDs == true) {
                $('[data-image-id]')
                .each(function () {
                    counter++;
                    $(this)
                    .attr('data-image-id', counter);
                });
            }
            $(setClickAttr)
                .on('click', function () {
                updateGallery($(this));
                });
            }
        });

        // build key actions
        $(document)
        .keydown(function (e) {
            switch (e.which) {
            case 37: // left
                if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                $('#show-previous-image')
                    .click();
                }
                break;

            case 39: // right
                if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                $('#show-next-image')
                    .click();
                }
                break;

            default:
                return; // exit this handler for other keys
            }
            e.preventDefault(); // prevent the default action (scroll / move caret)
        });

    </script>

@endsection