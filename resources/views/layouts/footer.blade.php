<!-- Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 footer-about wow fadeInUp">
                    {{-- <img class="logo-footer" src="images/logo.ico" alt="logo-footer" data-at2x="images/logo.ico"> --}}
                    <p>
                        Asociación Cultural Amigos del Pueblo Saharahui de Castilla y León en Valladolid
                    </p>
                    <p><a href="/asociate">Únete</a></p>
                    <p><a href="/donativos">Dona</a></p>
                </div>
                <div class="col-md-4 col-lg-4 offset-lg-1 footer-contact wow fadeInDown">
                    <h3>Contacto</h3>
                    <p><i class="fa fa-map-marker"></i>Plaza de las Nieves, 10, 3A 47011 Valladolid</p>
                    <p><i class="fa fa-phone"></i>657 805 775 </p>
                    <p><i class="fa fa-envelope"></i> Email: <a href="mailto:saharacyl@gmail.com">saharacyl@gmail.com</a></p>
                </div>
                <div class="col-md-4 col-lg-3 footer-social wow fadeInUp">
                    <h3>Síguenos</h3>
                    <p>
                        <a href="https://www.facebook.com/amigosdelpueblosaharauiASHVA" alt="Facebook"><i class="fa fa-facebook-square"></i></a> 
                        <a href="" alt="Suscríbete a nuestras noticias"><i class="fa fa-rss-square"></i></a> 
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                    <div class="col-md-5 footer-copyright">
                    <p>&copy; ASHVA {{ date('Y') }}</p>
                </div>
                {{-- <div class="col-md-7 footer-menu">
                    <nav class="navbar navbar-dark navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link scroll-link" href="#top-content">Top</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-link" href="#section-1">Section 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-link" href="#section-2">Section 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-link" href="#section-3">Section 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-link" href="#section-4">Section 4</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div> --}}
            </div>
        </div>
    </div>
</footer>
