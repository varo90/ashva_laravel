<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/eventos"><img src="{{ asset('images/logos/ashva.png') }}" height="90px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/eventos">Eventos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/about">La asociación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/imagenes">Imágenes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/historia">Historia</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Colabora
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/asociate">Asóciate</a>
                <a class="dropdown-item" href="/donativos">Haz un donativo</a>
                {{-- <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div> --}}
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/vacaciones-en-paz">Vacaciones en paz</a>
            </li>
        </ul>
        </div>
    </div>
</nav>