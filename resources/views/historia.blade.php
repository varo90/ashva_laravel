@extends('layouts.layout')

@section('title','Historia')

@section('content')

    <!-- About Section -->
    <section id="historia">
        <div class="contenedor">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Un poco de historia</h2>
                    <h3 class="section-subheading text-muted">Una breve explicaci&oacute;n sobre los hechos hist&oacute;ricos que han llevado a la lamentable situaci&oacute;n actual:</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/historia1.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>Los Inicios</h4>
                                    <h4 class="subheading">El conflicto saharaui comenz&oacute; como una lucha contra la colonia espa&ntilde;ola.</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">El Sahara Occidental era una colonia espa&ntilde;ola. A principios de los a&ntilde;os 70, siguiendo la ola de descolonizaci&oacute;n en el resto del continente, los saharauis comienzan a levantarse frente a la ocupaci&oacute;n espa&ntilde;ola. En 1974, Espa&ntilde;a promete a los saharauis un referendum de autodeterminaci&oacute;n.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/MarchaVerde.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>La traici&oacute;n</h4>
                                    <h4 class="subheading">Espa&ntilde;a traiciona a los saharauis y Marruecos inicia la ocupaci&oacute;n.</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">El 14 de noviembre de 1975, Marruecos y Mauritania firman con Espa&ntilde;a un acuerdo que nunca ser&iacute;a publicado en el Bolet&iacute;n Oficial del Estado. Espa&ntilde;a se compromete a retirarse del Sahara Occidental y ese territorio ser&iacute;a divido entre Marruecos y Mauritania. Los saharauis denuncian el acuerdo: rompe las promesas que Espa&ntilde;a les hab&iacute;a hecho, viola una resoluci&oacute;n del Tribunal Penal Internacional y los entrega a dos nuevos poderes coloniales. Pocas semanas despu&eacute;s, las tropas espa&ntilde;olas comienzan a retirarse. Marruecos y Mauritania lanzan sus ej&eacute;rcitos a la conquista del Sahara Occidental frente a la oposici&oacute;n de sus habitantes.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/saharauis-runnaway.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>La conquista marroqu&iacute;</h4>
                                    <h4 class="subheading">Bombardeos marroqu&iacute;es y hu&iacute;da de los saharauis a los campos de refugiados.</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Las fuerzas a&eacute;reas marroquies bombardean pueblos saharauis, utilizando en algunas ocasiones napalm y f&oacute;sforo blanco. Decenas de miles de saharauis huyen hacia Argelia, donde se construyen campos de refugiados en mitad del desierto. (Hoy, m&aacute;s de 35 a&ntilde;os despu&eacute;s, los saharauis siguen all&iacute;).</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/frente-polisario.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>La resistencia</h4>
                                    <h4 class="subheading">Guerra entre Marruecos y el Frente Polisario</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">El Frente Polisario plantea una guerra de guerrillas frente al potente ejercito marroqu&iacute; que recibe el apoyo de Francia y Estados Unidos. Mauritania acepta su derrota ante el Polisario en 1979 y reconoce la soberan&iacute;a del pueblo saharaui sobre el Sahara Occidental. Los combates entre Marruecos y el Frente Polisario se extienden durante 16 a&ntilde;os.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-image">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/mapa-sahara.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>El fin de la guerra</h4>
                                    <h4 class="subheading">Plan de paz de la ONU</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Marruecos y el Frente Polisario firman en 1991 el alto el fuego bajo auspicios de la ONU. Al final de la guerra, Marruecos controla un 80% del territorio del Sahara Occidental. Esto incluye la casi totalidad de las zonas pobladas, los dep&oacute;sitos de fosfatos y los ricos caladeros pesqueros. El 20% restante del Sahara Occidental, controlado por el Frente Polisario, es un terreno in&oacute;spito del desierto interior. Adem&aacute;s, Marruecos construy&oacute; una barrera, "el muro de la vergüenza", de m&aacute;s de 2.200 kil&oacute;metros que separa ambos territorios. A lo largo de ese muro se encuentra uno de los campo de minas m&aacute;s grandes del mundo. El plan de paz firmado por ambas partes estipula la creaci&oacute;n de un censo para celebrar un referendum en el que los saharauis puedan elegir entre la integraci&oacute;n en Marruecos o la independencia.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/contra.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>La esperanza</h4>
                                    <h4 class="subheading">El referendum que nunca se lleva a cabo</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">La ONU ten&iacute;a previsto realizar el censo en unas pocas semanas, pero debido a las continuas trabas impuestas por Marruecos, el censo tard&oacute; m&aacute;s de 8 a&ntilde;os completarse. Y cuando la ONU lo public&oacute; en el a&ntilde;o 2000, Marruecos se neg&oacute; a aceptarlo. Adem&aacute;s, desde la firma del alto el fuego en 1991, Marruecos incentiva a muchos de sus ciudadanos a mudarse al Sahara Occidental, incrementado as&iacute; sus opciones de &eacute;xito en una posible votaci&oacute;n. Hoy, m&aacute;s de 20 a&ntilde;os despu&eacute;s, el referendum todav&iacute;a no se ha celebrado. Ning&uacute;n pa&iacute;s del mundo reconoce el Sahara Ocidental como parte de Marruecos. Pero los intereses de sus aliados, hacen que Marruecos no sienta ninguna urgencia por encontrar una soluci&oacute;n.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-image">
                            <div class="timeline-image">
                                <img class="rounded-circle img-responsive" src="images/asociacion/muro-verguenza.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>Ahora...</h4>
                                    <h4 class="subheading">¿Cu&aacute;l es la situaci&oacute;n de los saharauis hoy?</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Unos 150.000 viven en la parte del Sahara Occidental bajo ocupaci&oacute;n marroqu&iacute;. Cientos de activistas pro-saharauis han sido detenidos y torturados por las autoridades de Marruecos. Unos 30.000 viven en la parte del Sahara Occidental controlada por el Polisario. Y otros 120.000 viven en los campos de refugiados construidos en el desierto de Argelia. Los refugiados dependen de la ayuda humanitaria internacional que se ha desplomado durante la crisis econ&oacute;mica. El Programa Mundial de Alimentos estima que dos tercios de las mujeres sufren anemia. Adem&aacute;s, la barrera construida por Marruecos hace que miles de familias sigan separadas despu&eacute;s de d&eacute;cadas.</p>
                                </div>
                            </div>
                        </li>
                        <!--<li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Be Part
                                    <br>Of Our
                                    <br>Story!</h4>
                            </div>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection