@extends('layouts.layout')

@section('title','Asóciate')

@section('content')

    <section id=socio class=bg-light-gray>
        <div align=center class=contenedor>
            <div class=row>
                <div class=col-lg-12>
                    <h1 class=section-heading&quot;>Asóciate</h1>
                    <h3 class=section-subheading text-muted>Siendo socio/a podr&aacute;s participar de primera mano en las variadas actividades y proyectos de la Asociaci&oacute;n de Amigos del Pueblo Saharahui de Valladolid. Tu colaboraci&oacute;n ser&aacute; muy preciada, y dado que siempre estamos en continuo movimiento y disponemos de actividades muy diversas en las que puedes participar en base a tus gustos y aficiones.</h3>
                </div>
            </div>
            <div><img src=images/asociacion/socios.jpg alt=Img></div><br>
            <div>
                <h3>&iquest;Quieres involucrarte?</h3>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    A&uacute;n nos queda un largo camino por recorrer. &iexcl;T&uacute; puedes marcar la diferencia!
                </p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     Si deseas hacerte socio/a tienes las siguientes opciones:<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Ll&aacute;manos: 657 805 775<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Escribe un email: saharacyl@gmail.com<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Imprime el siguiente formulario y entr&eacute;ganoslo personalmente o por carta: <a href="documentos/SOLICITUD DE ADMISION DE SOCIO.pdf" target="_blank">formulario</a>
                </p>
            </div>
        </div>
    </section>

@endsection