@extends('layouts.layout_admin')

@section('title','Crear evento')

@section('content')

{{-- <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> --}}
    <div class="contenedor">
        <h1>Crear nuevo evento</h1>
        @include('commons/errors')

        <form class="form-events" method="POST" action="/eventos" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <input class="form-control {{ $errors->has('titulo') ? 'ng-invalid' : '' }}" type="text" name="titulo" placeholder="Título" value="{{ old('titulo') }}" required>
            </div>
            <div class="form-group">
                <label for="desc_corta">Descripción corta:</label>
                <textarea id="desc_corta" rows="6" class="ckeditor form-control {{ $errors->has('desc_corta') ? 'ng-invalid' : '' }}" name="desc_corta" placeholder="Descripción corta" required>{{ old('desc_corta') }}</textarea>
            </div>
            <div class="form-group">
                <label for="desc_corta">Descripción larga:</label>
                <textarea id="desc_larga" class="ckeditor" rows="8" class="ckeditor form-control {{ $errors->has('desc_larga') ? 'ng-invalid' : '' }}" name="desc_larga" placeholder="Descripción larga">{{ old('desc_larga') }}</textarea>
            </div>
            <div class="form-group">
                <label for="author">Imagen:</label>
                <input type="file" class="form-control {{ $errors->has('imagen') ? 'ng-invalid' : '' }}" name="imagen"/>
            </div>
            <div>
                <button  class="btn btn-primary" type="submit">Crear evento</button>
            </div>
        </form>
    </div>

@endsection