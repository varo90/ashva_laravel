@extends('layouts.layout_admin')

@section('title','Editar evento')

@section('content')

    <div class="contenedor">
        <h1>Editar evento</h1>

        @include('commons/errors')
        
        <form class="form-events" method="POST" action="/eventos/{{$evento->id}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div>
                <input class="form-control {{ $errors->has('titulo') ? 'ng-invalid' : '' }}" type="text" name="titulo" placeholder="Título" value="{{ $errors->any() ? old('titulo') : $evento->titulo }}" required>
            </div>
            <div>
                <textarea rows="6" class="ckeditor form-control {{ $errors->has('desc_corta') ? 'ng-invalid' : '' }}" name="desc_corta" placeholder="Descripción corta" required>{{ $errors->any() ? old('desc_corta') : $evento->desc_corta }}</textarea>
            </div>
            <div>
                <textarea rows="8" class="ckeditor form-control {{ $errors->has('desc_larga') ? 'ng-invalid' : '' }}" name="desc_larga" placeholder="Descripción larga">{{ $errors->any() ? old('desc_larga') : $evento->desc_larga }}</textarea>
            </div>
            <div class="form-group">
                <label for="author">Imagen:</label>
                <input type="file" class="form-control {{ $errors->has('imagen') ? 'ng-invalid' : '' }}" name="imagen"/>
            </div>
            <div>
                <button class="btn btn-primary" type="submit">Editar evento</button>
            </div>
        </form>
    </div>

@endsection