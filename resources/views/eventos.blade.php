@extends('layouts.layout')

@section('title','Eventos y noticias')

@section('content')

    <div class="contenedor blog">
        <!-- Intro Section-->
        {{-- <section class="intro">
        <div class="container">
            <div class="row">
            <div class="col-lg-8">
                <h2 class="h3">Some great intro here</h2>
                <p class="text-big">Place a nice <strong>introduction</strong> here <strong>to catch reader's attention</strong>. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
            </div>
            </div>
        </div>
        </section> --}}
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Eventos y noticias</h2>
                <h3 class="section-subheading text-muted">Estas son las últimas noticias y eventos que queremos destacar:</h3>
            </div>
        </div>
        <section class="featured-posts no-padding-top">
            @if(Auth::check() && Auth::user()->role_id == 1)
                <a href="/eventos/create" class="btn btn-primary" role="button" aria-pressed="true">Crear nuevo</a>
            @endif
            <div class="container">
                <!-- Post-->
                @foreach($eventos as $evento)
                    <?php $impar = ( $loop->index == 1 || ($loop->index%2)!=0 ); ?>
                    <div class="row d-flex align-items-stretch {{ $impar ? 'lightgrey' : '' }}">
                        @if( $impar )
                            <div class="image col-lg-5">
                                <a href="/eventos/{{ $evento->id }}">
                                    <img src="images/portfolio/{{$evento->imagen}}">
                                </a>
                            </div>
                        @endif
                        <div class="text col-lg-7">
                            <div class="text-inner d-flex align-items-center">
                                <div class="content">
                                    <header class="post-header">
                                        {{-- <div class="category">
                                            <a href="#">Business</a>
                                            <a href="#">Technology</a>
                                        </div> --}}
                                        <a href="/eventos/{{ $evento->id }}">
                                            <h2 class="h4">{!! $evento->titulo !!}</h2>
                                        </a>
                                    </header>
                                    <p>{!! $evento->desc_corta !!}</p>
                                    <footer class="post-footer d-flex align-items-center">
                                        {{-- <a href="#" class="author d-flex align-items-center flex-wrap">
                                            <div class="avatar">
                                                <img src="https://d19m59y37dris4.cloudfront.net/blog/1-2/img/avatar-1.jpg" alt="..." class="img-fluid">
                                            </div>
                                            <div class="title">
                                                <span>John Doe</span>
                                            </div>
                                        </a> --}}
                                        <div class="date">
                                            <i class="fa fa-clock"></i>{{ date('d/m/Y',strtotime($evento->created_at)) }}
                                        </div>
                                        <div class="buttons">
                                            @if(Auth::check() && Auth::user()->role_id == 1)
                                                <a href="/eventos/{{ $evento->id }}/edit" class="btn btn-primary btn-sm" role="button">Editar</a>
                                                <form id="destroy_{{ $evento->id }}" method="POST" action="/eventos/{{$evento->id}}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    {{-- <button class="btn btn-danger btn-sm delete_evento" type="submit"><i class="fa fa-times"></i></button> --}}
                                                </form>
                                                <button id="{{ $evento->id }}" class="btn btn-danger btn-sm delete_evento" type="submit"><i class="fa fa-times"></i></button>
                                            @endif
                                        </div>
                                        {{-- <div class="comments">
                                            <i class="icon-comment"></i>12
                                        </div> --}}
                                    </footer>
                                </div>
                            </div>
                        </div>
                        @if( !$impar )
                            <div class="image col-lg-5">
                                <a href="/eventos/{{ $evento->id }}">
                                    <img src="images/portfolio/{{$evento->imagen}}">
                                </a>    
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </section>
    </div>

    <script type="text/javascript">
        $( ".delete_evento" ).on( "click", function() {
            var id = $(this).attr('id');
            bootbox.confirm({
                title: "Eliminar evento",
                message: "¿De verdad quieres eliminar el evento?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Sí'
                    }
                },
                callback: function (result) {
                    if(result) {
                        $( "#destroy_"+id ).submit();
                    }
                }
            });
        });
        // bootbox.alert("Hello world!");
    </script>
@endsection