<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = [
        'titulo',
        'desc_corta',
        'desc_larga',
        'imagen',
        'activo'
    ];
}
