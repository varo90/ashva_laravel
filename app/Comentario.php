<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = [
        'user',
        'comentario'
    ];

    public function event() {
        return $this->belongsTo(Evento::class);
    }

    public function owner() {
        return $this->belongsTo(User::class);
    }
}
