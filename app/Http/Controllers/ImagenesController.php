<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImagenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imagenes = $this->order_files_by_date_desc(\File::allFiles(public_path('images/camp')));
        return view('imagenes',compact('imagenes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = request()->validate([
            'imagenes' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        // $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=$file->getClientOriginalName();
                $file->move('images/camp/',$name);
                $images[]=$name;
            }
        }
    
        return redirect('/imagenes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {
        $image_path = "images/camp/".$name;  // Value is not URL but directory file path
        if(\File::exists($image_path)) {
            \File::delete($image_path);
        }
        return redirect('/imagenes');
    }

    public function order_files_by_date_desc($imagenes) {
        $imgs = array();
        foreach($imagenes as $key => $imagen) {
            $imgs[$imagen->getMTime().'_'.$key] = $imagen;
        }
        $imagenes = $imgs;
        krsort($imagenes);
        return $imagenes;
    }
}
