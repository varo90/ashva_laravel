<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EventosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = Evento::where('activo',1)->orderBy('created_at', 'DESC')->get();
        return view('eventos',compact('eventos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role_id!=1) {
            return redirect('/eventos');
        }
        return view('eventos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->role_id != 1) {
            return redirect('/eventos');
        }
        $attributes = $this->validate_data_form();
        if(request()->hasFile('imagen')){
            $attributes['imagen'] = $this->upload_image();
        } else {
            $attributes['imagen'] = 'logo.jpg';
        }

        $evento = Evento::create($attributes);

        return redirect('/eventos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Evento $evento)
    {
        return view('evento',compact('evento'));;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Evento $evento)
    {
        if(Auth::user()->role_id != 1) {
            return redirect('/eventos');
        }
        return view('eventos.edit',compact('evento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evento $evento)
    {
        if(Auth::user()->role_id != 1) {
            return redirect('/eventos');
        }
        $attributes = $this->validate_data_form();
        if(request()->hasFile('imagen')){
            $attributes['imagen'] = $this->upload_image();
        } else {
            $attributes['imagen'] = $evento->imagen;
        }

        $evento->update($attributes);

        return redirect('/eventos/'.$evento->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evento $evento)
    {
        if(Auth::user()->role_id != 1) {
            return redirect('/eventos');
        }
        $evento->delete();
        return redirect('/eventos');
    }

    public function validate_data_form() {
        return request()->validate([
            'titulo' => ['required','min:3','max:255'],  //LARAVEL VALIDATION RULES
            'desc_corta' => ['required','min:3','max:500'], //LARAVEL VALIDATION RULES
            'desc_larga' => ['max:4000'], //LARAVEL VALIDATION RULES
            'imagen' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    }

    public function upload_image() {
        if(request()->hasFile('imagen')){
            if (Input::file('imagen')->isValid()) {
                $file = Input::file('imagen');
                $destination = 'images/portfolio/';
                $ext = $file->getClientOriginalExtension();
                $mainFilename = str_random(6).date('h-i-s').'.'.$ext;
                $file->move($destination, $mainFilename);
                return $mainFilename;
            }
        }
    }

    public function is_not_admin() {
        if(Auth::user()->role_id != 1) {
            return redirect('/eventos');
        }
    }
}
