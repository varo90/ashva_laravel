<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage;

class AshvaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function about()
    {
        return view('about');
    }

    public function historia()
    {
        return view('historia');
    }

    public function vep()
    {
        return view('vacaciones-en-paz');
    }

    public function asociate()
    {
        return view('asociate');
    }

    public function donativos()
    {
        return view('donativos');
    }
}
